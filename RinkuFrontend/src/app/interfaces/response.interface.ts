export interface IResponse<T> {
    data: T,
    statusCode: number;
    message: string;
}

export interface IExisteMovimiento{
    value: boolean
}