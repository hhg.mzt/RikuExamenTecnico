
export interface IReporte {
    idEmpleado: number,
    empleado: string,
    rol: string,
    horasTrabajadas: number,
    bonoEntrega: number,
    bonoPorHora: number,
    isr: number,
    isrAdicional: number,
    vales: number
}
