export interface IRol{
    idRol: number,
    descripcion: string,
    activo: boolean,
    checked?: boolean;
}