export interface IMovimiento{
    idEmpleado: number,
    mes: number,
    year: number,
    cantidadEntregas: number
}

export interface IMovimientoRead extends IMovimiento{
    idMovimiento: number
}