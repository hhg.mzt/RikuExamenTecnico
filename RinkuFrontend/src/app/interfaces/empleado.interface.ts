export interface IEmpleado{
    numero: number,
    nombreCompleto: string,
    idRol: number
}

export interface IEmpleadoRead extends IEmpleado{
    idEmpleado: number
}