import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable} from 'rxjs';
import { IEmpleado, IEmpleadoRead } from '../interfaces/empleado.interface';
import { IResponse } from '../interfaces/response.interface';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class EmpleadosService {
  urlApi: string = `${environment.apiUlr}/Empleados`;
  constructor(private _http: HttpClient) { }

  getEmpleados(): Observable<IResponse<IEmpleadoRead[]>>{
    return this._http.get<IResponse<IEmpleadoRead[]>>(this.urlApi);
  }

  grabar(empleado: IEmpleado): Observable<IResponse<IEmpleado>>{
    return this._http.post<IResponse<IEmpleado>>(this.urlApi, empleado);
  }

  edit(empleado: IEmpleadoRead, empleadoId: number): Observable<IResponse<IEmpleado>>{
    return this._http.put<IResponse<IEmpleado>>(`${this.urlApi}/${empleadoId}`, empleado);
  }

  remove( empleadoId: number):  Observable<IResponse<IEmpleado>>{
    return this._http.delete<IResponse<IEmpleado>>(`${this.urlApi}/${empleadoId}`);
  }

}
