import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable} from 'rxjs';
import { IMovimiento } from '../interfaces/movimiento.interface';
import { IReporte } from '../interfaces/reporte.interface';

import { IResponse, IExisteMovimiento } from '../interfaces/response.interface';
import { environment } from 'src/environments/environment';
@Injectable({
  providedIn: 'root'
})
export class MovimientoService {

  urlApi: string = `${environment.apiUlr}/Movimiento`;
  constructor(private _http: HttpClient) { }

  getReporte(mes: number, year: number): Observable<IResponse<IReporte[]>>{
    return this._http.get<IResponse<IReporte[]>>(`${this.urlApi}/mes/${mes}/year/${year}/reporte`);
  }

  grabar(movimiento: IMovimiento):  Observable<IResponse<IMovimiento>>{
    return this._http.post<IResponse<IMovimiento>>(`${this.urlApi}`, movimiento);
  }

  verficiar(empleadoId: number, mes: number, year: number): Observable<IExisteMovimiento>{
    let url: string = `${this.urlApi}/empleado/${empleadoId}/mes/${mes}/year/${year}/verificar`;
    return this._http.get<IExisteMovimiento>(`${this.urlApi}/empleado/${empleadoId}/mes/${mes}/year/${year}/verificar`);
  }

  eliminarMovimiento(empleadoId: number, mes: number, year: number): Observable<IExisteMovimiento>{
    return this._http.post<IExisteMovimiento>(`${this.urlApi}/empleado/${empleadoId}/mes/${mes}/year/${year}/eliminar-movimiento`, {});
  }

}
