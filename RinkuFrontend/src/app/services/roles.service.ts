import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { IRol } from '../interfaces/rol.interface';
import { IResponse } from '../interfaces/response.interface';
import { environment } from 'src/environments/environment';
@Injectable({
  providedIn: 'root'
})
export class RolesService {
  urlApi: string = `${environment.apiUlr}/Roles`;

  constructor(private _http: HttpClient) { }

  traerRoles(): Observable<IResponse<IRol[]>>{
    return this._http.get<IResponse<IRol[]>>(this.urlApi);
  }

}
