import { Component, OnInit, ViewChild  } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { IEmpleadoRead } from 'src/app/interfaces/empleado.interface';
import { EmpleadosService } from 'src/app/services/empleados.service';
import { EmpleadoModalComponent } from '../../modals/empleado-modal/empleado-modal.component';
import { SwalMensaje } from '../../../utility/swalmensaje';
import { SnackMsgService } from '../../../utility/snack-msg.service';
@Component({
  selector: 'app-list-empleados',
  templateUrl: './list-empleados.component.html',
  styleUrls: ['./list-empleados.component.css']
})
export class ListEmpleadosComponent implements OnInit {
  empleados: IEmpleadoRead[] = [];
  columnasTabla: string[] = ["numero", "nombreCompleto", "idRol", "acciones"];
  dataInicio: IEmpleadoRead[] = [];
  dataListaEmpleados = new MatTableDataSource(this.dataInicio);
  @ViewChild(MatPaginator) paginacionTabla!: MatPaginator;

  constructor(private empleadoService: EmpleadosService,
    private dialog: MatDialog,
    private snackBarService: SnackMsgService) { }

  ngOnInit(): void {
    this.traerEmpleados();
  }

  ngAfterViewInit(): void {
    this.dataListaEmpleados.paginator = this.paginacionTabla;
  }


  aplicarFiltroTabla(event: Event){
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataListaEmpleados.filter = filterValue.trim().toLocaleLowerCase();
  }

  traerEmpleados(){
    this.empleadoService.getEmpleados().subscribe({
      next: (resp) => {
        if(resp.statusCode === 200){
          this.dataListaEmpleados.data = resp.data; 
        }
      }
    });
  }

  agregarEmpleado(){
    this.dialog.open(EmpleadoModalComponent, {
      disableClose: true,
    }).afterClosed().subscribe(res => {
      if(res === true) this.traerEmpleados();
    });
  }

  edit(empleado: IEmpleadoRead){
    this.dialog.open(EmpleadoModalComponent, {
      disableClose: true,
      data: empleado
    }).afterClosed().subscribe(res => {
      if(res === true) this.traerEmpleados();
    });
  }

  remove(empleadoId: number){
    const msg: string = `Deseas eliminar al empleado con id: ${empleadoId}`;
    SwalMensaje.mostrarPregunta("Eliminar", msg, "Sí, eliminar").then((resp) => {
      if(resp.isConfirmed){
        this.empleadoService.remove(empleadoId).subscribe({
          next: (resp) => {
            if(resp.statusCode === 200){
              this.snackBarService.mostrarMsg("Se elimino", "empleados");
              this.traerEmpleados();
            }
          }, error : errr => {
            this.snackBarService.mostrarMsg("Ocurrio un error al eliminar", "empleados")
          }
        });

      }
    });

  }

}
