import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { IReporte } from 'src/app/interfaces/reporte.interface';
import { MovimientoService } from 'src/app/services/movimiento.service';
import { MovimientoModalComponent } from '../../modals/movimiento-modal/movimiento-modal.component';
import { SwalMensaje } from '../../../utility/swalmensaje';
import { SnackMsgService } from '../../../utility/snack-msg.service';

@Component({
  selector: 'app-list-movimientos',
  templateUrl: './list-movimientos.component.html',
  styleUrls: ['./list-movimientos.component.css']
})
export class ListMovimientosComponent implements OnInit {
  meses: { nombre: string; numero: number }[] = [
    { nombre: 'Enero', numero: 1 },
    { nombre: 'Febrero', numero: 2 },
    { nombre: 'Marzo', numero: 3 },
    { nombre: 'Abril', numero: 4 },
    { nombre: 'Mayo', numero: 5 },
    { nombre: 'Junio', numero: 6 },
    { nombre: 'Julio', numero: 7 },
    { nombre: 'Agosto', numero: 8 },
    { nombre: 'Septiembre', numero: 9 },
    { nombre: 'Octubre', numero: 10 },
    { nombre: 'Noviembre', numero: 11 },
    { nombre: 'Diciembre', numero: 12 },
  ];
  mesSeleccionado: number = 0;
  reporte: IReporte[] = [];
  columnasTabla: string[] = ["idEmpleado", "empleado", "rol", "horasTrabajadas", "bonoEntrega", "bonoPorHora", "isr", "isrAdicional", "vales"];
  dataInicio: IReporte[] = [];
  dataReporte = new MatTableDataSource(this.dataInicio);
  @ViewChild(MatPaginator) paginacionTabla!: MatPaginator;

  fechaActual: Date = new Date();
  year: number = 0;



  constructor(
    private movimientoService: MovimientoService,
    private dialog: MatDialog,
    private snackBarService: SnackMsgService
  ) { }

  ngOnInit(): void {
    this.year = this.fechaActual.getFullYear();
  }

  ngAfterViewInit(): void {
    this.dataReporte.paginator = this.paginacionTabla;
  }

  aplicarFiltroTabla(event: Event){
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataReporte.filter = filterValue.trim().toLocaleLowerCase();
  }


  traerReporte(){
    this.movimientoService.getReporte(this.mesSeleccionado, this.year).subscribe({
      next: (resp) => {
        if (resp.statusCode === 200){
          this.dataReporte.data = resp.data;
        }
      }
    });
  }

  changeMes($event: any) {
    
    if($event.value > 0){
      this.mesSeleccionado = $event.value;
      this.dataReporte.data = [];
      this.traerReporte();
    }
  }

  agregarMovimiento(){
    this.dialog.open(MovimientoModalComponent, {
      disableClose: true,
    }).afterClosed().subscribe(res => {
      if(res === true) this.traerReporte();
    });
  }


}
