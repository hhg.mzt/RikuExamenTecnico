import { Component, OnInit, Inject } from '@angular/core';
import { Form, FormBuilder, FormGroup, Validators } from "@angular/forms";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { IMovimiento } from 'src/app/interfaces/movimiento.interface';
import { IEmpleado, IEmpleadoRead } from 'src/app/interfaces/empleado.interface';
import { IRol } from 'src/app/interfaces/rol.interface';
import { EmpleadosService } from 'src/app/services/empleados.service';
import { RolesService } from 'src/app/services/roles.service';
import { SnackMsgService } from 'src/app/utility/snack-msg.service';
import { MovimientoService } from 'src/app/services/movimiento.service';
import { SwalMensaje } from 'src/app/utility/swalmensaje';  
@Component({
  selector: 'app-movimiento-modal',
  templateUrl: './movimiento-modal.component.html',
  styleUrls: ['./movimiento-modal.component.css']
})
export class MovimientoModalComponent implements OnInit {
  empleados: IEmpleadoRead[] = [];
  roles: IRol[] = [];
  numEmpleado: number = 0;
  rol: string = "";
  idEmpleadoSelected: number = 0;
  numEntregas: number = 0;
  mes?: string = "";
  numeroMes: number = 0;
  fechaActual: Date = new Date();
  year: number = 0;

  meses: { nombre: string; numero: number }[] = [
    { nombre: 'Enero', numero: 1 },
    { nombre: 'Febrero', numero: 2 },
    { nombre: 'Marzo', numero: 3 },
    { nombre: 'Abril', numero: 4 },
    { nombre: 'Mayo', numero: 5 },
    { nombre: 'Junio', numero: 6 },
    { nombre: 'Julio', numero: 7 },
    { nombre: 'Agosto', numero: 8 },
    { nombre: 'Septiembre', numero: 9 },
    { nombre: 'Octubre', numero: 10 },
    { nombre: 'Noviembre', numero: 11 },
    { nombre: 'Diciembre', numero: 12 },
  ];

  constructor(
    private empleadoService: EmpleadosService,
    private movimientoService: MovimientoService,
    private rolService: RolesService,
    private snackBarService: SnackMsgService,
    private modalActual: MatDialogRef<MovimientoModalComponent>,
    @Inject(MAT_DIALOG_DATA)
    public datosEmpleado: IEmpleadoRead,
    private fb: FormBuilder
  ) { }

  ngOnInit(): void {
    this.traerEmpleados();
    this.traerRoles();
    this.year = this.fechaActual.getFullYear();
    this.numeroMes = this.fechaActual.getMonth() + 1;
    this.mes = this.meses.find((m) => {
      return m.numero == this.numeroMes
    })?.nombre;
  }

  traerEmpleados(){
    this.empleadoService.getEmpleados().subscribe({
      next: (resp) => {
        if(resp.statusCode === 200){
          this.empleados = resp.data;
        }
      }
    })
  }

  traerRoles(){
    this.rolService.traerRoles().subscribe({
      next: (resp) => {
        if(resp.statusCode === 200){
          this.roles = resp.data;
        }
      }
    });
  }


  Guardar(){
    let interrumpido: boolean = false;
    const movimiento: IMovimiento = {
      idEmpleado: this.idEmpleadoSelected,
      mes: this.numeroMes,
      year: this.year,
      cantidadEntregas: this.numEntregas
    };
    this.movimientoService.verficiar(this.idEmpleadoSelected, this.numeroMes, this.year).subscribe({
      next: (resp) => {
        if(resp.value === true){
          SwalMensaje.mostrarPregunta("Ya existe", "Ya existe un movimiento guardado para este emplead, ¿Deseas sobrescribir la informacion?", "Sí, Sobrescribir").then((res) => {
            if(res.isConfirmed){
              this.movimientoService.eliminarMovimiento(this.idEmpleadoSelected, this.numeroMes, this.year).subscribe({
                next: (resp)=>{
                  if(resp.value == true){
                    this.enviarPost(movimiento);
                  }
                }
              });
            }else{
              interrumpido = true;
            }
          });
        }
        else{
          this.enviarPost(movimiento);
        }
        
      }
    });

  }

  enviarPost(movimiento: IMovimiento){
    this.movimientoService.grabar(movimiento).subscribe({
      next: (resp) => {
        if(resp.statusCode === 201){
          this.snackBarService.mostrarMsg("se guardo con exito", "movimientos");
        }
        else{
          this.snackBarService.mostrarMsg("Intenta más tarde ocurrio un error", "movimientos");
        }
        this.modalActual.close(true);
      }
    })
  }

  changeEmpleado($event: any) {
    
    if($event.value > 0){
      this.idEmpleadoSelected = $event.value;
      const empleado = this.empleados.find((empleado) =>{
        return empleado.idEmpleado===this.idEmpleadoSelected;
      });
      const rol = this.roles.find((rol) => {
        return rol.idRol === empleado?.idRol
      });

      if(empleado){
        this.numEmpleado = empleado?.numero;
      }
      
      if(rol){
        console.log(rol?.descripcion);
        this.rol = rol?.descripcion;
        console.log(rol.descripcion);
      }
    }
  }

}
