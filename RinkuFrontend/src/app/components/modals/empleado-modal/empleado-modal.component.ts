import { Component, OnInit, Inject } from '@angular/core';
import { Form, FormBuilder, FormGroup, Validators } from "@angular/forms";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";

import { IEmpleado, IEmpleadoRead } from 'src/app/interfaces/empleado.interface';
import { IRol } from 'src/app/interfaces/rol.interface';
import { EmpleadosService } from 'src/app/services/empleados.service';
import { RolesService } from 'src/app/services/roles.service';
import { SnackMsgService } from 'src/app/utility/snack-msg.service';

@Component({
  selector: 'app-empleado-modal',
  templateUrl: './empleado-modal.component.html',
  styleUrls: ['./empleado-modal.component.css']
})
export class EmpleadoModalComponent implements OnInit {
  formularioEmpleado: FormGroup;
  tituloAccion: string = "Agregar";
  botonAccion: string = "Guardar";
  listaRoles: IRol[] = [];
  idRolSelected: number = 0;
  cargarRoles: boolean = false;

  constructor(
    private empleadoService: EmpleadosService,
    private rolService: RolesService,
    private snackBarService: SnackMsgService,
    private modalActual: MatDialogRef<EmpleadoModalComponent>,
    @Inject(MAT_DIALOG_DATA)
    public datosEmpleado: IEmpleadoRead,
    private fb: FormBuilder
  ) {
    this.formularioEmpleado = fb.group({
      numero: ["", Validators.required ],
      nombreCompleto: ["", Validators.required ]
    });
   }

  ngOnInit(): void {
    this.traerRoles();
    if(this.datosEmpleado != null){
      this.mostrarEmpleado();
      this.idRolSelected = this.datosEmpleado.idRol;
    }
  }

  mostrarEmpleado(){
    this.formularioEmpleado.patchValue({
      numero: this.datosEmpleado.numero,
      nombreCompleto: this.datosEmpleado.nombreCompleto
    });
  }

  traerRoles(){
    this.rolService.traerRoles().subscribe({
      next: (resp) => {
        if(resp.statusCode === 200){
          this.listaRoles = resp.data;
          this.listaRoles.forEach((rol) => {
            rol.checked = false;
          });
          this.cargarRoles = true;
          this.handleRol();
        }
      }
    });
  }

  guardarOEditar(){
    if(this.datosEmpleado == null)
    {
      const empleadoNuevo: IEmpleado = {
        numero: this.formularioEmpleado.value.numero,
        nombreCompleto: this.formularioEmpleado.value.nombreCompleto,
        idRol: this.idRolSelected
      };
  
      this.empleadoService.grabar(empleadoNuevo).subscribe({
        next: resp => {
          if(resp.statusCode === 201){
            this.snackBarService.mostrarMsg("Se guardo un nuevo empleado", "empleados");
            this.modalActual.close(true);
          }
        }, 
        error: err => {
          console.error(err)
          this.snackBarService.mostrarMsg("Ocurrio un error al guardar, porfavor intenta más tarde", "empleados");
        }
      });
    }
    else 
    {
      const empleado: IEmpleadoRead = {
        idEmpleado: this.datosEmpleado.idEmpleado,
        numero: this.formularioEmpleado.value.numero,
        nombreCompleto: this.formularioEmpleado.value.nombreCompleto,
        idRol: this.idRolSelected 
      }

      this.empleadoService.edit(empleado, this.datosEmpleado.idEmpleado).subscribe({
        next: (resp) => {
          if(resp.statusCode == 200){
            this.snackBarService.mostrarMsg("Se actualizo el empleado", "empleados");
            this.modalActual.close(true);
          }
        }, 
        error: err => {
          console.error(err)
          this.snackBarService.mostrarMsg("Ocurrio un error al actualizar, porfavor intenta más tarde", "empleados");
        }
      })

    }

  }

  changeRol($event: any) {
    this.idRolSelected = $event.value;
  }

  handleRol(){
    if(this.datosEmpleado != null)
    {
      const rol = this.listaRoles.find((rol) => { return rol.idRol === this.datosEmpleado.idRol});
      if(rol){
        const position = this.listaRoles.indexOf(rol);
        this.listaRoles[position].checked = true;
        this.idRolSelected = rol.idRol;
      }
    }
    else
    {
      this.listaRoles[0].checked = true;
      this.idRolSelected = this.listaRoles[0].idRol;
    }
  }

}
