import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ListEmpleadosComponent } from './components/pages/list-empleados/list-empleados.component';
import { ListMovimientosComponent } from './components/pages/list-movimientos/list-movimientos.component';

const routes: Routes = [
  {path: "empleados", component: ListEmpleadosComponent},
  {path: "movimientos", component: ListMovimientosComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
