﻿namespace RinkuBackend.Utils
{
    public static class CalcularNomina
    {
        public static DesgloceNomina ObtenerDesgloce(float sueldo, int horas, int dias_semana, int entregas_hechas,
                int bonoPorEntrega, int bonoPorHora, float porcenatajeIsrSimple,
                float porcenatajeIsrAdicional, float sueldoParaAplicarAdicional, float porcentajeVale)
        { 
            float sueldoMensual = (sueldo * horas * dias_semana) * 4;
            //bonos por entrega y hora
            float bonosEntregas = (bonoPorEntrega * entregas_hechas);
            float bonosHoras  =  ((horas * dias_semana * 4) * bonoPorHora);
            //
            float sueldoMasBonos = sueldoMensual + bonosEntregas + bonosHoras;
            //calcular isr simple 
            float isrSimple = sueldoMasBonos * (porcenatajeIsrSimple / 100);

            float isrAdicional = sueldoMasBonos >= sueldoParaAplicarAdicional ? sueldoMasBonos * (porcenatajeIsrAdicional / 100) : 0;
            float montoVales = sueldoMensual * (porcentajeVale / 100);
            int horasTrabajadas = bonoPorHora == 0 ? 192 : (int)bonosHoras / bonoPorHora;
            return new DesgloceNomina() { 
                SueldoMensual = sueldoMensual,
                TotalBonoEntrega = bonosEntregas,
                TotalBonoHora = bonosHoras, 
                Isr = isrSimple,
                IsrAdicional = isrAdicional,
                MontoVales = montoVales,
                HorasTrabajadas = horasTrabajadas
            };
        }
    }


    public class DesgloceNomina
    {
        public float SueldoMensual { get; set; }
        public float TotalBonoEntrega { get; set; }
        public float TotalBonoHora { get; set; }
        public float Isr { get; set; }
        public float IsrAdicional { get; set; }
        public float MontoVales { get; set; }
        public int HorasTrabajadas { get; set; }
    }

}
