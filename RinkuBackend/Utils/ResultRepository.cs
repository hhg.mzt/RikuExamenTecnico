namespace RinkuBackend.Utils
{
    public class ResultContainer<TData>
    {
        public TData? Result { get; set; }
        public bool HasError { get; set; }
        public string? Error { get; set; }
    }
}