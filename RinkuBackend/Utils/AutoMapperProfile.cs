﻿using AutoMapper;
using RinkuBackend.Models;
using RinkuBackend.DTO;

namespace RinkuBackend.Utils
{
    public class AutoMapperProfile: Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<CatBonoPorHora, CatBonoPorHoraDTO>().ReverseMap();
            CreateMap<CatBonoPorHora, CatBonoPorHoraReadDTO>().ReverseMap();

            CreateMap<CatBonosPorEntrega, BonoPorEntregaDTO>().ReverseMap();
            CreateMap<CatBonosPorEntrega, BonoPorEntegaReadDTO>().ReverseMap();

            CreateMap<CatEmpleado, CatEmpleadoDTO>().ReverseMap();
            CreateMap<CatEmpleado, CatEmpleadoReadDTO>().ReverseMap();

            CreateMap<CatIsrCalculo, CatIsrCalculoDTO>().ReverseMap();
            CreateMap<CatIsrCalculo, CatIsrCalculoReadDTO>().ReverseMap();

            CreateMap<CatOtrasPrestacione, CatOtrasPrestacioneDTO>().ReverseMap();
            CreateMap<CatOtrasPrestacione, CatOtrasPrestacioneReadDTO>().ReverseMap();

            CreateMap<CatRole, CatRoleDTO>().ReverseMap();
            CreateMap<CatRole, CatRoleReadDTO>().ReverseMap();

            CreateMap<Movimiento, MovimientoDTO>().ReverseMap();
            CreateMap<Movimiento, MovimientoReadDTO>().ReverseMap();

            CreateMap<PagoMensual, PagoMensualDTO>().ReverseMap();
            CreateMap<PagoMensual, PagoMensualReadDTO>().ReverseMap();

            CreateMap<Reporte, ReporteDTO>().ReverseMap();
        }
    }
}
