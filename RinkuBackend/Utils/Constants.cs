﻿namespace RinkuBackend.Utils
{
    public static class Constants
    {
        public static int SUELDO = 30;
        public static int HORAS_JORNADA = 8;
        public static int DIAS_POR_SEMANA = 6;
        public static int BONO_ENTREGA = 5;
        public static int BONO_HORA_CHOFER = 10;
        public static int BONO_HORA_CARGADOR = 5;
        public static int BONO_HORA_AUXILIAR = 0;
        public static int PORCENTAJE_ISR = 9;
        public static int PORCENTAJE_ISR_ADICIONAL = 3;
        public static int SUELDO_MENSUAL_ISR_ADICIONAL = 10000;
        public static int PORCENTAJE_VALES = 4;
        public static int SEMANAS_POR_MES = 4;
    }
}
