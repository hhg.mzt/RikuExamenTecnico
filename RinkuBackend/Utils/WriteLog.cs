using System;
using System.IO;

namespace RinkuBackend.Utils
{
    public class WriteLog
    {
        public string LogMessage { get; set; }
        public bool ShowConsole { get; set; }

        public WriteLog(string logMessage, bool showConsole)
        {
            LogMessage = logMessage;
            if(showConsole)
                showMessageConsole();
            writleLineFile();
        }

        public WriteLog()
        {
            if(ShowConsole)
                showMessageConsole();
            writleLineFile();
        }

        public void showMessageConsole()
        {
            //Quitar posibles saltos de línea del mensaje
            LogMessage = LogMessage.Replace(Environment.NewLine, " | ");
            LogMessage = LogMessage.Replace("\r\n", " | ").Replace("\n", " | ").Replace("\r", " | ");
            Console.WriteLine(DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss") + " " + LogMessage);
        }

        //Escribe el mensaje de la propiedad mensajeLog en un fichero en la carpeta del ejecutable
        public void writleLineFile()
        {
            try
            {
                FileStream fs = new FileStream(@AppDomain.CurrentDomain.BaseDirectory +
                    "estado.log", FileMode.OpenOrCreate, FileAccess.Write);
                StreamWriter m_streamWriter = new StreamWriter(fs);
                m_streamWriter.BaseStream.Seek(0, SeekOrigin.End);
                //Quitar posibles saltos de línea del mensaje
                LogMessage = LogMessage.Replace(Environment.NewLine, " | ");
                LogMessage = LogMessage.Replace("\r\n", " | ").Replace("\n", " | ").Replace("\r", " | ");                
                m_streamWriter.WriteLine(DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss") + " " + LogMessage);
                m_streamWriter.Flush();
                m_streamWriter.Close();
            }
            catch(Exception ex)
            {
                //Silenciosa
            }
        }

    }
}
