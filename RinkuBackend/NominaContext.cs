﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using RinkuBackend.Models;

namespace RinkuBackend
{
    public partial class NominaContext : DbContext
    {
        public NominaContext(DbContextOptions<NominaContext> options) : base(options) { }

        public virtual DbSet<CatBonoPorHora> CatBonoPorHoras { get; set; } = null!;
        public virtual DbSet<CatBonosPorEntrega> CatBonosPorEntregas { get; set; } = null!;
        public virtual DbSet<CatEmpleado> CatEmpleados { get; set; } = null!;
        public virtual DbSet<CatIsrCalculo> CatIsrCalculos { get; set; } = null!;
        public virtual DbSet<CatOtrasPrestacione> CatOtrasPrestaciones { get; set; } = null!;
        public virtual DbSet<CatRole> CatRoles { get; set; } = null!;
        public virtual DbSet<Movimiento> Movimientos { get; set; } = null!;

        public virtual DbSet<PagoMensual> PagosMensuales { get; set; } = null!;

        public virtual DbSet<Reporte> Reportes { get; set; } = null!;
    
    }
}
