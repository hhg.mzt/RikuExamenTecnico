USE [rinku_nominas]
GO
/****** Object:  StoredProcedure [dbo].[ActualizarEmpleado]    Script Date: 19/07/2023 01:08:10 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[ActualizarEmpleado]
    @IdEmpleado INT,
    @Numero INT,
    @NombreCompleto VARCHAR(70),
    @IdRol INT
AS
BEGIN
    UPDATE CatEmpleados
    SET Numero = @Numero,
        NombreCompleto = @NombreCompleto,
        IdRol = @IdRol
    WHERE IdEmpleado = @IdEmpleado
END


USE [rinku_nominas]
GO
/****** Object:  StoredProcedure [dbo].[GrabarMovimiento]    Script Date: 19/07/2023 09:31:58 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[GrabarMovimiento]
	@IdMovimiento INT OUTPUT,
    @IdEmpleado INT,
    @Mes TINYINT,
    @Year SMALLINT,
    @CantidadEntregas SMALLINT
AS
BEGIN
    INSERT INTO Movimientos (IdEmpleado, Mes, Year, CantidadEntregas)
    VALUES (@IdEmpleado, @Mes, @Year, @CantidadEntregas)

	SET @IdMovimiento = SCOPE_IDENTITY()
END

USE [rinku_nominas]
GO
/****** Object:  StoredProcedure [dbo].[GrabarEmpleado]    Script Date: 19/07/2023 09:29:28 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[GrabarEmpleado]
	@IdEmpleado INT OUTPUT,
    @Numero INT,
    @NombreCompleto VARCHAR(70),
    @IdRol INT
AS
BEGIN
    INSERT INTO CatEmpleados (Numero, NombreCompleto, IdRol)
    VALUES (@Numero, @NombreCompleto, @IdRol)

	SET @IdEmpleado = SCOPE_IDENTITY()
END

USE [rinku_nominas]
GO
/****** Object:  StoredProcedure [dbo].[ActualizarMovimiento]    Script Date: 19/07/2023 01:14:14 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[ActualizarMovimiento]
    @IdMovimiento INT,
    @IdEmpleado INT,
    @Mes TINYINT,
    @Year SMALLINT,
    @CantidadEntregas SMALLINT
AS
BEGIN
    UPDATE Movimientos
    SET IdEmpleado = @IdEmpleado,
        Mes = @Mes,
        Year = @Year,
        CantidadEntregas = @CantidadEntregas
    WHERE IdMovimiento = @IdMovimiento
END


USE [rinku_nominas]
GO
/****** Object:  StoredProcedure [dbo].[ConsultarEmpleados]    Script Date: 19/07/2023 01:19:21 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[ConsultarEmpleados] AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * From CatEmpleados;
END

CREATE PROCEDURE Reporte 
	-- Add the parameters for the stored procedure here
	@Mes INT,
	@Year INT
AS
BEGIN
	Select 
		Movimientos.IdEmpleado, CatEmpleados.NombreCompleto As Empleado,
		CatRoles.Descripcion As Rol,
		sum(pagosmensuales.HorasTrabajadas) As HorasTrabajadas,
		sum(pagosmensuales.TotalBonoEntrega) as BonoEntrega,
		sum(pagosmensuales.TotalBonoHora) as BonoPorHora, 
		sum(pagosmensuales.TotalIsr) As Isr,
		sum(pagosmensuales.TotalIsrAdicional) as IsrAdicional,
		sum(pagosmensuales.TotalVales) as Vales
	from pagosmensuales
	inner join movimientos on (movimientos.IdMovimiento = pagosmensuales.IdMovimiento)
	left join CatEmpleados on (movimientos.IdEmpleado = CatEmpleados.IdEmpleado)
	left join CatRoles on (CatEmpleados.IdRol = CatRoles.IdRol)
	left join catBonoPorHora on (catBonoPorHora.IdRol = CatEmpleados.IdRol)
	Where
		Movimientos.Mes = @Mes and Movimientos.Year = @Year
	group by
	Movimientos.IdEmpleado, CatEmpleados.NombreCompleto, CatEmpleados.IdRol,
	CatRoles.Descripcion

END
GO