USE [rinku_nominas]
GO
/****** Object:  Table [dbo].[CatBonoPorHora]    Script Date: 19/07/2023 01:45:46 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CatBonoPorHora](
	[IdBonoHora] [int] IDENTITY(1,1) NOT NULL,
	[IdRol] [int] NOT NULL,
	[Bono] [decimal](3, 0) NOT NULL,
	[FechaInicio] [date] NOT NULL,
	[FechaFin] [date] NOT NULL,
	[Activo] [bit] NOT NULL,
 CONSTRAINT [PK_CatBonoPorHora] PRIMARY KEY CLUSTERED 
(
	[IdBonoHora] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CatBonosPorEntrega]    Script Date: 19/07/2023 01:45:46 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CatBonosPorEntrega](
	[IdBonoEntrega] [int] IDENTITY(1,1) NOT NULL,
	[Bono] [decimal](3, 0) NOT NULL,
	[FechaInicio] [date] NOT NULL,
	[FechaFin] [date] NOT NULL,
	[Activo] [bit] NOT NULL,
 CONSTRAINT [PK_CatBonosPorEntrega] PRIMARY KEY CLUSTERED 
(
	[IdBonoEntrega] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CatEmpleados]    Script Date: 19/07/2023 01:45:46 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CatEmpleados](
	[IdEmpleado] [int] IDENTITY(1,1) NOT NULL,
	[Numero] [int] NOT NULL,
	[NombreCompleto] [varchar](70) NOT NULL,
	[IdRol] [int] NOT NULL,
 CONSTRAINT [PK_CatEmpleados] PRIMARY KEY CLUSTERED 
(
	[IdEmpleado] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CatIsrCalculos]    Script Date: 19/07/2023 01:45:46 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CatIsrCalculos](
	[IdIsr] [int] IDENTITY(1,1) NOT NULL,
	[FactorSimple] [numeric](3, 0) NOT NULL,
	[FactorAdicional] [numeric](2, 0) NOT NULL,
	[SueldoParaAplicarAdional] [smallint] NOT NULL,
	[FechaInicio] [date] NOT NULL,
	[FechaFin] [date] NOT NULL,
 CONSTRAINT [PK_CatIsrCalculos] PRIMARY KEY CLUSTERED 
(
	[IdIsr] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CatOtrasPrestaciones]    Script Date: 19/07/2023 01:45:46 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CatOtrasPrestaciones](
	[IdPrestacion] [smallint] IDENTITY(1,1) NOT NULL,
	[Descripcion] [varchar](30) NOT NULL,
	[Factor] [smallint] NULL,
	[SobreSueldo] [bit] NOT NULL,
	[AdiconalASueldo] [bit] NOT NULL,
	[FechaInicio] [date] NOT NULL,
	[FechaFin] [date] NOT NULL,
 CONSTRAINT [PK_CatOtrasPrestaciones] PRIMARY KEY CLUSTERED 
(
	[IdPrestacion] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CatRoles]    Script Date: 19/07/2023 01:45:47 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CatRoles](
	[IdRol] [int] IDENTITY(1,1) NOT NULL,
	[Descripcion] [varchar](20) NOT NULL,
	[Activo] [bit] NOT NULL,
 CONSTRAINT [PK_CatRoles] PRIMARY KEY CLUSTERED 
(
	[IdRol] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Movimientos]    Script Date: 19/07/2023 01:45:47 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Movimientos](
	[IdMovimiento] [int] IDENTITY(1,1) NOT NULL,
	[IdEmpleado] [int] NOT NULL,
	[Mes] [tinyint] NOT NULL,
	[Year] [smallint] NOT NULL,
	[CantidadEntregas] [smallint] NOT NULL,
 CONSTRAINT [PK_Movientos] PRIMARY KEY CLUSTERED 
(
	[IdMovimiento] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PagosMensuales]    Script Date: 19/07/2023 01:45:47 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PagosMensuales](
	[IdPagoMensual] [int] IDENTITY(1,1) NOT NULL,
	[IdMovimiento] [int] NOT NULL,
	[SueldoMensual] [numeric](10, 0) NOT NULL,
	[TotalBonoEntrega] [numeric](10, 0) NOT NULL,
	[TotalBonoHora] [numeric](10, 0) NULL,
	[TotalIsr] [numeric](10, 0) NOT NULL,
	[TotalIsrAdicional] [numeric](10, 0) NOT NULL,
	[TotalVales] [numeric](10, 0) NULL,
 CONSTRAINT [PK_PagosMensuales] PRIMARY KEY CLUSTERED 
(
	[IdPagoMensual] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CatEmpleados]  WITH CHECK ADD  CONSTRAINT [IdRol] FOREIGN KEY([IdRol])
REFERENCES [dbo].[CatRoles] ([IdRol])
GO
ALTER TABLE [dbo].[CatEmpleados] CHECK CONSTRAINT [IdRol]
GO
