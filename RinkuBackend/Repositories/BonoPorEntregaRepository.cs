﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using RinkuBackend.DTO;
using RinkuBackend.Repositories.Contracts;

namespace RinkuBackend.Repositories
{
    public class BonoPorEntregaRepository : IBonoPorEntrega
    {
        private readonly NominaContext _context;
        private readonly IMapper _mapper;

        public BonoPorEntregaRepository(NominaContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        public async Task<BonoPorEntegaReadDTO> Buscar(DateTime fechaActual)
        {
            var bonoPorEntrega = await _context.CatBonosPorEntregas
                .Where(bono => bono.FechaInicio <= fechaActual && bono.FechaFin >= fechaActual)
                .FirstOrDefaultAsync();
            return _mapper.Map<BonoPorEntegaReadDTO>(bonoPorEntrega);
        }
    }
}
