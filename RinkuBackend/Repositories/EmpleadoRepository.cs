﻿using System;
using AutoMapper;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Serilog;
using RinkuBackend.DTO;
using RinkuBackend.Repositories.Contracts;
using RinkuBackend.Utils;
using System.Data;

namespace RinkuBackend.Repositories
{
    public class EmpleadoRepository : IEmpleadoRepository
    {
        private readonly NominaContext _nominaContext;
        private readonly IMapper _mapper;
        public EmpleadoRepository(NominaContext nominaContext, IMapper mapper)
        {
            _nominaContext = nominaContext ??
                throw new ArgumentNullException(nameof(nominaContext));
            _mapper = mapper;
        }

        public ResultContainer<int> Grabar(CatEmpleadoDTO empleado) //Task<CatEmpleadoReadDTO>
        {
            var idEmpleadoParam = new SqlParameter("@IdEmpleado", SqlDbType.Int)
            {
                Direction = ParameterDirection.Output
            };
            var result = new ResultContainer<int>();
            try
            {
                var numeroParam = new SqlParameter("@Numero", empleado.Numero);
                var nombreCompletoParam = new SqlParameter("@NombreCompleto", empleado.NombreCompleto);
                var idRolParam = new SqlParameter("@IdRol", empleado.IdRol);

                _nominaContext.Database.ExecuteSqlRaw("EXEC [dbo].[GrabarEmpleado] @IdEmpleado OUTPUT, @Numero, @NombreCompleto, @IdRol",
                    idEmpleadoParam, numeroParam, nombreCompletoParam, idRolParam);

                var idGenerado = (int)idEmpleadoParam.Value;
                result.Result = idGenerado;
                result.HasError = false;
                result.Error = "";
            }catch (SqlException ex)
            {            
                //throw new Exception($"Error en base de datos: {ex.Message}");
                result.Result = 0;
                result.HasError = true;
                result.Error = $"Error en base de datos: {ex.Message}";
                new WriteLog($"Error en base de datos: {ex.Message} [EmpleadoRepository - Gravar Linea 50]", true);
            }
            return result;
        }
        public async Task<List<CatEmpleadoReadDTO>> Consular()
        {
            var empleados = await _nominaContext.CatEmpleados
                .FromSqlRaw("EXEC [dbo].[ConsultarEmpleados]")
                .ToListAsync();
            return _mapper.Map<List<CatEmpleadoReadDTO>>(empleados);
        }

        public async Task<CatEmpleadoReadDTO> ConsultarPorId(int id)
        {
            var empleado = await _nominaContext.CatEmpleados
                .Where(e => e.IdEmpleado == id)
                .FirstOrDefaultAsync();

            return _mapper.Map<CatEmpleadoReadDTO>(empleado);
        }

        public Task<CatEmpleadoReadDTO> Actualizar(CatEmpleadoReadDTO empleado)
        {
            var idParam = new SqlParameter("@IdEmpleado", empleado.IdEmpleado);
            var numeroParam = new SqlParameter("@Numero", empleado.Numero);
            var nombreCompletoParam = new SqlParameter("@NombreCompleto", empleado.NombreCompleto);
            var idRolParam = new SqlParameter("@IdRol", empleado.IdRol);

            _nominaContext.Database.ExecuteSqlRaw("EXEC [dbo].[ActualizarEmpleado] @IdEmpleado, @Numero, @NombreCompleto, @IdRol",
                idParam, numeroParam, nombreCompletoParam, idRolParam);

            return ConsultarPorId(empleado.IdEmpleado);
        }
        public  CatEmpleadoReadDTO Remover(int id)
        {
            var empleado = _nominaContext.CatEmpleados.Find(id);
            if(empleado is null)
                throw new Exception("No existe el empleado");
            var empleadoMapper = _mapper.Map<CatEmpleadoReadDTO>(empleado);
            _nominaContext.Remove(empleado);
            _nominaContext.SaveChanges();
            return empleadoMapper;
        }

    }
}
