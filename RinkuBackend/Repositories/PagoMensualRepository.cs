﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using RinkuBackend.DTO;
using RinkuBackend.Models;
using RinkuBackend.Repositories.Contracts;

namespace RinkuBackend.Repositories
{
    public class PagoMensualRepository : IPagoMensualRepository
    {
        private readonly NominaContext _context;
        private readonly IMapper _mapper;

        public PagoMensualRepository(NominaContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<PagoMensualReadDTO> Crear(PagoMensualDTO pago)
        {
            try
            {
                var pagoModelo = _mapper.Map<PagoMensual>(pago);
                _context.Set<PagoMensual>().Add(pagoModelo);
                await _context.SaveChangesAsync();
                return _mapper.Map<PagoMensualReadDTO>(pagoModelo);
            }
            catch
            {
                throw new Exception("Ocurrio un error al guardar el pago mensual");
            }
        }
        public async Task<PagoMensualReadDTO> Buscar(int idMovimiento)
        {
            var pago = await _context.PagosMensuales
                .Where(p => p.IdMovimiento == idMovimiento)
                .FirstOrDefaultAsync();
            return _mapper.Map<PagoMensualReadDTO>(pago);
        }
        
    }
}
