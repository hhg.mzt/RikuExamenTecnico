﻿
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using RinkuBackend.DTO;
using RinkuBackend.Repositories.Contracts;

namespace RinkuBackend.Repositories
{
    public class IsrCalculoRepository: IIsrCalculoService
    {
        private readonly NominaContext _context;
        private readonly IMapper _mapper;
        public IsrCalculoRepository(NominaContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<CatIsrCalculoReadDTO> Buscar(DateTime fechaActual)
        {
            var isrCalculo = await _context.CatIsrCalculos
                .Where(c => c.FechaInicio <= fechaActual && c.FechaFin >= fechaActual)
                .FirstOrDefaultAsync();
            return _mapper.Map<CatIsrCalculoReadDTO>(isrCalculo);
;        }
    }
}
