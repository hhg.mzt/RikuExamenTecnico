﻿using AutoMapper;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using RinkuBackend.DTO;
using RinkuBackend.Models;
using RinkuBackend.Repositories.Contracts;
using System.ComponentModel;
using System.Data;

namespace RinkuBackend.Repositories
{
    public class MovimientoRepository : IMovimientoRepository
    {
        private readonly NominaContext _context;
        private readonly IBonoPorHoraRepository _bonoPorHoraRepository;
        private readonly IMapper _mapper;

        public MovimientoRepository(NominaContext context, IMapper mapper, IBonoPorHoraRepository bonoPorHoraRepository)
        {
            _context = context;
            _mapper = mapper;
            _bonoPorHoraRepository = bonoPorHoraRepository;
        }
        public async Task<MovimientoReadDTO> Grabar(MovimientoDTO movimiento)
        {
            var idMovimientoParam = new SqlParameter("@IdMovimiento", SqlDbType.Int)
            {
                Direction = ParameterDirection.Output
            };

            var idEmpleadoParam = new SqlParameter("@IdEmpleado", movimiento.IdEmpleado);
            var mesParam = new SqlParameter("@Mes", movimiento.Mes);
            var yearParam = new SqlParameter("@Year", movimiento.Year);
            var cantidadEntregasParam = new SqlParameter("@CantidadEntregas", movimiento.CantidadEntregas);
            _context.Database.ExecuteSqlRaw("EXEC [dbo].[GrabarMovimiento] @IdMovimiento OUTPUT, @IdEmpleado, @Mes, @Year, @CantidadEntregas",
                idMovimientoParam, idEmpleadoParam, mesParam, yearParam, cantidadEntregasParam);

            var idGenerado = (int)idMovimientoParam.Value;
            return await ConsultarPorId(idGenerado);            
        }

        public async Task<List<MovimientoReadDTO>> Consular()
        {
            var bono = await _bonoPorHoraRepository.Buscar(DateTime.Now, 1);
            var mesParam  = new SqlParameter("@Mes", DateTime.Now.Month);
            var movimientos  = await _context.Movimientos
              .FromSqlRaw("EXEC [dbo].[ConsultarMovimiento] @Mes", mesParam)
              .ToListAsync();
            return _mapper.Map<List<MovimientoReadDTO>>(movimientos);

        }

        public async Task<MovimientoReadDTO> ConsultarPorId(int id)
        {
            var movimiento = await _context.Movimientos.Where(mov => mov.IdMovimiento == id).FirstOrDefaultAsync();
            return _mapper.Map<MovimientoReadDTO>(movimiento);
        }
        /*public Task<MovimientoReadDTO> Actualizar(MovimientoReadDTO empleado)
        {
            throw new NotImplementedException();
        }*/ 
        public async Task<List<ReporteDTO>> Reporte(int mes, int year)
        {
            var connectionString = _context.Database.GetConnectionString();
            using (var connection = new SqlConnection(connectionString))
            {
                await connection.OpenAsync();

                using (var command = new SqlCommand("Reporte", connection))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.Add(new SqlParameter("@Mes", mes));
                    command.Parameters.Add(new SqlParameter("@Year", year));

                    var result = new List<ReporteDTO>();
                    using (var reader = await command.ExecuteReaderAsync())
                    {
                        while (reader.Read())
                        {
                            var reporte = new ReporteDTO
                            {
                                IdEmpleado = Convert.ToInt32(reader.GetValue(1)),
                                Empleado = (string) reader.GetValue(2),
                                Rol = (string) reader.GetValue(3),
                                HorasTrabajadas = Convert.ToInt32(reader.GetValue(4)),
                                BonoEntrega = Convert.ToInt32(reader.GetValue(5)),
                                BonoPorHora = Convert.ToInt32(reader.GetValue(6)),
                                Isr = Convert.ToSingle(reader.GetValue(7)),
                                IsrAdicional = Convert.ToSingle(reader.GetValue(8)),
                                Vales = Convert.ToSingle(reader.GetValue(9))
                            };
                            result.Add(reporte);         
                        }
                    }

                    return result;
                }
            }
        }

        public Boolean Buscar(int idEmpleado, int mes, int year)
        {
            var movimiento = _context.Movimientos
                .Where(m => m.IdEmpleado == idEmpleado && m.Mes == mes && m.Year == year)
                .Select(m => m.IdEmpleado)
                .FirstOrDefault();
            return (movimiento != 0);
        }
        public void EliminarMovimiento(int idEmpleado, int mes, int year)
        {
            var movimiento = _context.Movimientos
                .Where(m => m.IdEmpleado == idEmpleado && m.Mes == mes && m.Year == year)
                .FirstOrDefault();
            if(movimiento != null)
            {
              
                _context.Remove(movimiento);
                _context.SaveChanges();

            }
        }

       
    }
}
