﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using RinkuBackend.DTO;
using RinkuBackend.Repositories.Contracts;

namespace RinkuBackend.Repositories
{
    public class BonoPorHoraRepository : IBonoPorHoraRepository
    {
        private readonly NominaContext _context;
        private readonly IMapper _mapper;

        public BonoPorHoraRepository(NominaContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<CatBonoPorHoraReadDTO> Buscar(DateTime fechaActual, int idRol)
        {
            var bonoPorHora = await _context.CatBonoPorHoras
                .Where(bono => bono.FechaInicio <= fechaActual && bono.FechaFin >= fechaActual && bono.IdRol == idRol)
                .FirstOrDefaultAsync();

            return _mapper.Map<CatBonoPorHoraReadDTO>(bonoPorHora);
        }
    }
}
