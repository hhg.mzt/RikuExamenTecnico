﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using RinkuBackend.DTO;
using RinkuBackend.Repositories.Contracts;

namespace RinkuBackend.Repositories
{
    public class OtrasPrestacionesRepository : IOtrasPrestacionesRepository
    {
        private readonly NominaContext _context;
        private readonly IMapper _mapper;

        public OtrasPrestacionesRepository(NominaContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        public async Task<CatOtrasPrestacioneReadDTO> Buscar(DateTime fechaActual)
        {
            var otraPrestacion = await _context.CatOtrasPrestaciones
                .Where(op => op.Descripcion == "VALES" && op.FechaInicio <= fechaActual && op.FechaFin >= fechaActual)
                .FirstOrDefaultAsync();
            return _mapper.Map<CatOtrasPrestacioneReadDTO>(otraPrestacion);
        }
    }
}
