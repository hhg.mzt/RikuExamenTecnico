﻿using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Linq.Expressions;

using RinkuBackend.Models;
using RinkuBackend.Repositories.Contracts;

namespace RinkuBackend.Repositories
{
    public class RolRepository : IRolRepository
    {
        private readonly NominaContext _nominaContext;
        public RolRepository(NominaContext nominaContext)
        {
            _nominaContext = nominaContext ??
                throw new ArgumentNullException(nameof(nominaContext));
        }
        public async Task<List<CatRole>> Fetch()
        {
            return await _nominaContext.CatRoles.ToListAsync();
        }
    }
}
