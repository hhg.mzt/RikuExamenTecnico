﻿using RinkuBackend.Models;
using System.Linq.Expressions;

namespace RinkuBackend.Repositories.Contracts
{
    public interface IRolRepository
    {
        Task<List<CatRole>> Fetch();
    }
}
