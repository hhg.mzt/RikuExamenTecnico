﻿using RinkuBackend.DTO;

namespace RinkuBackend.Repositories.Contracts
{
    public interface IIsrCalculoService
    {
        Task<CatIsrCalculoReadDTO> Buscar(DateTime fechaActual);
    }
}
