﻿using RinkuBackend.DTO;

namespace RinkuBackend.Repositories.Contracts
{
    public interface IBonoPorEntrega
    {
        Task<BonoPorEntegaReadDTO> Buscar(DateTime fechaActual);
    }
}
