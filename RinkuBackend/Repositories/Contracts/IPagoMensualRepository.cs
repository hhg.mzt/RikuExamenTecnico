﻿using RinkuBackend.DTO;
namespace RinkuBackend.Repositories.Contracts
{
    public interface IPagoMensualRepository
    {
        Task<PagoMensualReadDTO> Crear(PagoMensualDTO pago);
        Task<PagoMensualReadDTO> Buscar(int idMovimiento);
    }
}
