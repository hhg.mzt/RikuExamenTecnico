﻿using RinkuBackend.DTO;

namespace RinkuBackend.Repositories.Contracts
{
    public interface IOtrasPrestacionesRepository
    {
        Task<CatOtrasPrestacioneReadDTO> Buscar(DateTime fechaActual);
    }
}
