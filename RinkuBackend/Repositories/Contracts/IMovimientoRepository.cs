﻿using RinkuBackend.DTO;

namespace RinkuBackend.Repositories.Contracts
{
    public interface IMovimientoRepository
    {
        Task<MovimientoReadDTO> Grabar(MovimientoDTO movimiento);
        Task<List<MovimientoReadDTO>> Consular();
        Task<MovimientoReadDTO> ConsultarPorId(int id);
        Task<List<ReporteDTO>> Reporte(int mes, int year);
        Boolean Buscar(int idEmpleado, int mes, int year);
        void EliminarMovimiento(int idEmpleado, int mes, int year);
        //Task<MovimientoReadDTO> Actualizar(MovimientoReadDTO empleado);
    }
}
