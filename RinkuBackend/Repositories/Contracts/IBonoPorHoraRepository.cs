﻿using RinkuBackend.DTO;

namespace RinkuBackend.Repositories.Contracts
{
    public interface IBonoPorHoraRepository
    {
        Task<CatBonoPorHoraReadDTO> Buscar(DateTime fechaActual, int idRol);
    }
}
