﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using AutoMapper;
using RinkuBackend.DTO;
using RinkuBackend.Services;
using RinkuBackend.Services.Contracts;
using RinkuBackend.Utils;

namespace RinkuBackend.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EmpleadosController : ControllerBase
    {
        private readonly IEmpleadoService _empleadoService;

        public EmpleadosController(IEmpleadoService empleadoService)
        {
            _empleadoService = empleadoService;
        }

        [HttpPost]
        public async Task<IActionResult> Grabar([FromBody] CatEmpleadoDTO empleadoDTO)
        {
          
            var result = _empleadoService.Grabar(empleadoDTO);
            var resp = new Response<CatEmpleadoReadDTO>();

            if(result.HasError){
                resp.StatusCode = 500;
                resp.Message = $"Ha ocurrido un error: {result.Error}";
                return BadRequest(resp);
            }

            resp.Data = await _empleadoService.ConsultarPorId(result.Result);
            resp.Message = "success";
            resp.StatusCode = 201;
            return Ok(resp);
        }
        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            var empleados = await _empleadoService.Consular();
            var resp = new Response<List<CatEmpleadoReadDTO>>();
            if (empleados.Count == 0)
            {
                resp.StatusCode = 404;
                resp.Message = "No se encontrarón empleados";
                return NotFound(resp);
            }
            resp.Data = empleados;
            resp.Message = "success";
            resp.StatusCode = 200;
            return Ok(resp);
        }

        [HttpGet("{id:int}")]
        public async Task<IActionResult> Consultar(int id)
        {
            var empleado = await _empleadoService.ConsultarPorId(id);
            var rsp = new Response<CatEmpleadoReadDTO>();
            if(empleado is null)
            {
                rsp.Message = "No se encontró el empleado";
                rsp.StatusCode = 404;
                return NotFound(rsp);
            }
            rsp.Message = "success";
            rsp.Data = empleado;
            rsp.StatusCode = 200;
            return Ok(rsp);
        }

        [HttpPut("{id:int}")]
        public async Task<IActionResult> Edit(CatEmpleadoReadDTO empleado)
        {
            var empleadoActualizado = await _empleadoService.Actualizar(empleado);
            var rsp = new Response<CatEmpleadoReadDTO>();
            rsp.Data = empleadoActualizado;
            rsp.Message = "success";
            rsp.StatusCode = 200;
            return Ok(rsp);
        }

        [HttpDelete("{id:int}")]
        public async Task<IActionResult> Remove(int id)
        {
            var empleadoEliminado = _empleadoService.Remover(id);
            var rsp = new Response<CatEmpleadoReadDTO>();
            if(empleadoEliminado is null)
            {
                rsp.Message = "No se encontró el empleado";
                rsp.StatusCode = 404;
                return NotFound(rsp);
            }
            rsp.Data = empleadoEliminado;
            rsp.Message = "success";
            rsp.StatusCode = 200;
            return Ok(rsp);
        }

    }
}
