﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RinkuBackend.Models;
using RinkuBackend.Services.Contracts;
using RinkuBackend.Utils;

namespace RinkuBackend.Controllers
{
    [Route("api/[controller]")]
    [ApiController]

    public class RolesController : ControllerBase
    {
        private readonly IRolService _rolService;
        public RolesController(IRolService rolService)
        {
            _rolService = rolService;
        }

        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            var resp = new Response<List<CatRole>>();
            var roles = await _rolService.Fetch();
            if (roles.Count == 0)
            {
                resp.Message = "No se encontrarón roles";
                resp.StatusCode = 404;
                return NotFound(resp);
            }
            resp.Data = roles;
            resp.Message = "success";
            resp.StatusCode = 200;
            return Ok(resp);           
        }




    }
}
