﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RinkuBackend.DTO;
using RinkuBackend.Services.Contracts;
using RinkuBackend.Utils;

namespace RinkuBackend.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MovimientoController : ControllerBase
    {
        private readonly IMovimientoService _movimientoService;
        public MovimientoController(IMovimientoService movimientoService)
        {
            _movimientoService = movimientoService;
        }

        [HttpPost]
        public async Task<IActionResult> Grabar([FromBody] MovimientoDTO movimiento)
        {
            var nuevoMovimiento = await _movimientoService.Grabar(movimiento);
            var resp = new Response<MovimientoReadDTO>();
            resp.Data = nuevoMovimiento;
            resp.Message = "succcess";
            resp.StatusCode = 201;
            return Ok(resp);
        }

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            var bono = await _movimientoService.Consular();
            return Ok(bono);
        }

        [HttpGet("mes/{mes:int}/year/{year:int}/reporte")]
        public async Task<IActionResult> Reporte(int mes, int year)
        {
            var resp = new Response<List<ReporteDTO>>();
            var reporte = await _movimientoService.Reporte(mes, year);
            if(reporte.Count == 0){
                resp.StatusCode = 404;
                resp.Message = "No hay reporte";
                return  NotFound(resp);
            }
            resp.Data = reporte;
            resp.Message = "success";
            resp.StatusCode = 200;
            return Ok(resp);
        }


        [HttpGet("empleado/{id:int}/mes/{mes:int}/year/{year:int}/verificar")]
        public async Task<IActionResult> Reporte(int id, int mes, int year)
        {
            var existe = _movimientoService.Buscar(id, mes, year);
            return Ok(new { value = existe});

        }

        [HttpPost("empleado/{id:int}/mes/{mes:int}/year/{year:int}/eliminar-movimiento")]
        public IActionResult EliminarMovimiento(int id, int mes, int year)
        {
            _movimientoService.EliminarMovimiento(id, mes, year);
            return Ok(new { value = true });
        }


    }
}
