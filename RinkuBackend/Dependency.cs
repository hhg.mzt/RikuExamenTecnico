﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

using RinkuBackend.Repositories;
using RinkuBackend.Repositories.Contracts;
using RinkuBackend.Services;
using RinkuBackend.Services.Contracts;
using RinkuBackend.Utils;

namespace RinkuBackend
{
    public static class Dependency
    {
        public static void DependencyInjection(this IServiceCollection services, IConfiguration configuration)
        {
            //SE INYECTA LA DEPENDECIA DE ENTITY FRAMEWORK
            services.AddDbContext<NominaContext>(opt =>
            {
                opt.UseSqlServer(configuration.GetConnectionString("BDConnection"));
            });

            services.AddAutoMapper(typeof(AutoMapperProfile));

            //repositorios
            services.AddScoped<IRolRepository, RolRepository>();
            services.AddScoped<IEmpleadoRepository, EmpleadoRepository>();
            services.AddScoped<IMovimientoRepository, MovimientoRepository>();
            services.AddScoped<IBonoPorHoraRepository, BonoPorHoraRepository>();
            services.AddScoped<IBonoPorEntrega, BonoPorEntregaRepository>();
            services.AddScoped<IOtrasPrestacionesRepository, OtrasPrestacionesRepository>();
            services.AddScoped<IIsrCalculoService, IsrCalculoRepository>();
            services.AddScoped<IPagoMensualRepository, PagoMensualRepository>();
            //servicios
            services.AddScoped<IRolService, RolService>();
            services.AddScoped<IEmpleadoService, EmpleadoService>();
            services.AddScoped<IMovimientoService, MovimientoService>();

        }
    }
}
