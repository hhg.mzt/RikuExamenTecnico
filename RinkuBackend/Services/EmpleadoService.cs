﻿using RinkuBackend.DTO;
using RinkuBackend.Repositories.Contracts;
using RinkuBackend.Services.Contracts;
using RinkuBackend.Utils;


namespace RinkuBackend.Services
{
    public class EmpleadoService : IEmpleadoService
    {
        private readonly IEmpleadoRepository _empleadoRepository;

        public EmpleadoService(IEmpleadoRepository empleadoRepository)
        {
            _empleadoRepository = empleadoRepository;
        }  
        public  ResultContainer<int> Grabar(CatEmpleadoDTO empleado)
        {
            var nuevoEmpleado =  _empleadoRepository.Grabar(empleado);
            return nuevoEmpleado;
        }
        public async  Task<CatEmpleadoReadDTO> ConsultarPorId(int id)
        {
            return await _empleadoRepository.ConsultarPorId(id);
        }
        public async Task<List<CatEmpleadoReadDTO>> Consular()
        {
            return await _empleadoRepository.Consular();
        }
        public async Task<CatEmpleadoReadDTO> Actualizar(CatEmpleadoReadDTO empleado)
        {
            var empleadoCambiado = await _empleadoRepository.Actualizar(empleado);
            return empleadoCambiado;
        }

        public  CatEmpleadoReadDTO Remover(int id)
        {
            return  _empleadoRepository.Remover(id);
        }

    }
}
