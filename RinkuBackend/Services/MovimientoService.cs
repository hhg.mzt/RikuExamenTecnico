﻿using RinkuBackend.DTO;
using RinkuBackend.Services.Contracts;
using RinkuBackend.Repositories.Contracts;
using RinkuBackend.Repositories;
using RinkuBackend.Utils;
using Microsoft.AspNetCore.Identity;
using RinkuBackend.Models;
using System.Numerics;

namespace RinkuBackend.Services
{
    public class MovimientoService : IMovimientoService
    {
        private readonly IMovimientoRepository _movimientoRepository;
        private readonly IBonoPorHoraRepository _bonoPorHoraRepository;
        private readonly IBonoPorEntrega _bonoPorEntregaRepository;
        private readonly IOtrasPrestacionesRepository _otrasPrestacionesRepository;
        private readonly IIsrCalculoService _isrCalculoRepository;
        private readonly IEmpleadoRepository _empleadoRepository;
        private readonly IPagoMensualRepository _pagoMensualRepository;
        public MovimientoService(IMovimientoRepository movimientoRepository,
            IBonoPorHoraRepository bonoPorHoraRepository,
            IBonoPorEntrega bonoPorEntregaRepository,
            IOtrasPrestacionesRepository otrasPrestacionesRepository,
            IIsrCalculoService isrCalculoRepository,
            IEmpleadoRepository empleadoRepository,
            IPagoMensualRepository pagoMensualRepository)
        {
            _movimientoRepository = movimientoRepository;
            _bonoPorHoraRepository = bonoPorHoraRepository;
            _bonoPorEntregaRepository = bonoPorEntregaRepository;
            _otrasPrestacionesRepository = otrasPrestacionesRepository;
            _isrCalculoRepository = isrCalculoRepository;
            _empleadoRepository = empleadoRepository;
            _pagoMensualRepository = pagoMensualRepository;
        }
        public async Task<MovimientoReadDTO> Grabar(MovimientoDTO movimiento)
        {
            try
            {
                // Obtendremos las valores para los calculos
                var empleado = await _empleadoRepository.ConsultarPorId(movimiento.IdEmpleado);
                if (empleado is null)
                    throw new Exception("Lo sentimos el empleado no existe");
                var idRol = empleado.IdRol;
                var bonoHora = await _bonoPorHoraRepository.Buscar(DateTime.Now, idRol);
                var bonoEntrega = await _bonoPorEntregaRepository.Buscar(DateTime.Now);
                var otrasPrestaciones = await _otrasPrestacionesRepository.Buscar(DateTime.Now);
                var calculoIsr = await _isrCalculoRepository.Buscar(DateTime.Now);
                var desgloceNomina = CalcularNomina.ObtenerDesgloce(Constants.SUELDO, Constants.HORAS_JORNADA, Constants.DIAS_POR_SEMANA, movimiento.CantidadEntregas,
                    int.Parse(bonoEntrega.Bono.ToString()), int.Parse(bonoHora.Bono.ToString()), float.Parse(calculoIsr.FactorSimple.ToString()),
                    float.Parse(calculoIsr.FactorAdicional.ToString()), float.Parse(calculoIsr.SueldoParaAplicarAdional.ToString()), float.Parse(otrasPrestaciones.Factor.ToString())
                    );
               
                var nuevoMovimiento = await _movimientoRepository.Grabar(movimiento);
                var newPagoMensual = new PagoMensualDTO() {
                    IdMovimiento = nuevoMovimiento.IdMovimiento,
                    SueldoMensual = (decimal)(desgloceNomina?.SueldoMensual),
                    TotalBonoEntrega = (decimal)(desgloceNomina?.TotalBonoEntrega),
                    TotalBonoHora = (decimal)(desgloceNomina?.TotalBonoHora),
                    TotalIsr = (decimal)(desgloceNomina?.Isr),
                    TotalIsrAdicional = (decimal)(desgloceNomina?.IsrAdicional),
                    TotalVales = (decimal)(desgloceNomina?.MontoVales),
                    HorasTrabajadas = (int)(desgloceNomina?.HorasTrabajadas)
                };
                _pagoMensualRepository.Crear(newPagoMensual);
                return nuevoMovimiento;
            }
            catch
            {
                throw;
            }
        }

        public async Task<List<MovimientoReadDTO>> Consular()
        {
            int idRol = 1;
            var bonoPorHora = await _bonoPorHoraRepository.Buscar(DateTime.Now, idRol);
            var movimientos = await _movimientoRepository.Consular();
            return movimientos;
        }

        public async Task<MovimientoReadDTO> ConsultarPorId(int id)
        {
            var movimiento = await _movimientoRepository.ConsultarPorId(id);
            return movimiento;
        }

        public async Task<List<ReporteDTO>> Reporte(int mes, int year)
        {
            return await _movimientoRepository.Reporte(mes, year);
        }
        public Boolean Buscar(int idEmpleado, int mes, int year){
            return _movimientoRepository.Buscar(idEmpleado, mes, year);
        }
        public void EliminarMovimiento(int idEmpleado, int mes, int year)
        {
            _movimientoRepository.EliminarMovimiento(idEmpleado, mes, year);
        }
        /*public Task<MovimientoReadDTO> Actualizar(MovimientoReadDTO empleado)
        {
            throw new NotImplementedException();
        }*/

    }
}
