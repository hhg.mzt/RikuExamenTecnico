﻿using RinkuBackend.DTO;
using RinkuBackend.Utils;

namespace RinkuBackend.Services.Contracts
{
    public interface IEmpleadoService
    {
        ResultContainer<int> Grabar(CatEmpleadoDTO empleado);
        Task<List<CatEmpleadoReadDTO>> Consular();
        Task<CatEmpleadoReadDTO> ConsultarPorId(int id);
        Task<CatEmpleadoReadDTO> Actualizar(CatEmpleadoReadDTO empleado);
        CatEmpleadoReadDTO Remover(int id);
    }
}
