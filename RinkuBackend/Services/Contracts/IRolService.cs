﻿using RinkuBackend.Models;
namespace RinkuBackend.Services.Contracts
{
    public interface IRolService
    {
        Task<List<CatRole>> Fetch();
    }
}
