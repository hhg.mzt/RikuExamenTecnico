﻿using Microsoft.EntityFrameworkCore;
using RinkuBackend.Services.Contracts;
using RinkuBackend.Repositories.Contracts;
using RinkuBackend.Models;

namespace RinkuBackend.Services
{
    public class RolService : IRolService
    {
        private readonly IRolRepository _rolRepository;

        public RolService(IRolRepository rolRepository)
        {
            _rolRepository = rolRepository;
        }

        public async Task<List<CatRole>> Fetch()
        {
            var roles = await _rolRepository.Fetch();       
            return roles;
        }
    }
}
