﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace RinkuBackend.Models
{
    [Table("Movimientos")]
    public partial class Movimiento
    {
        [Key]
        public int IdMovimiento { get; set; }
        public int IdEmpleado { get; set; }
        public byte Mes { get; set; }
        public short Year { get; set; }
        public short CantidadEntregas { get; set; }
    }
}
