﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace RinkuBackend.Models
{
    [Table("CatEmpleados")]
    public partial class CatEmpleado
    {
        [Key]
        public int IdEmpleado { get; set; }
        public int Numero { get; set; }
        public string NombreCompleto { get; set; } = null!;
        public int IdRol { get; set; }
    }
}
