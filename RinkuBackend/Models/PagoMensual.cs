﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace RinkuBackend.Models
{
    [Table("PagosMensuales")]
    public class PagoMensual
    {
        [Key]
        public int IdPagoMensual { get; set; }
        public int IdMovimiento { get; set; }
        public decimal SueldoMensual { get; set; }
        public decimal TotalBonoEntrega { get; set; }
        public decimal TotalBonoHora { get; set; }
        public decimal TotalIsr { get; set; }
        public decimal TotalIsrAdicional { get; set; }
        public decimal? TotalVales { get; set; }
        public int HorasTrabajadas { get; set; }
    }
}
