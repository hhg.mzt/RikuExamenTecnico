﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace RinkuBackend.Models
{
    [Table("CatOtrasPrestaciones")]
    public partial class CatOtrasPrestacione
    {
        [Key]
        public short IdPrestacion { get; set; }
        public string Descripcion { get; set; } = null!;
        public short Factor { get; set; }
        public bool SobreSueldo { get; set; }
        public bool AdiconalAsueldo { get; set; }
        public DateTime FechaInicio { get; set; }
        public DateTime FechaFin { get; set; }
    }
}
