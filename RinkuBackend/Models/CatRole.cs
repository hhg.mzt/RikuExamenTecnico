﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace RinkuBackend.Models
{
    [Table("CatRoles")]
    public  class CatRole
    {
        [Key]
        public int IdRol { get; set; }
        public string Descripcion { get; set; } = null!;
        public bool Activo { get; set; }

        //public virtual ICollection<CatEmpleado> CatEmpleados { get; set; }
    }
}
