using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace RinkuBackend.Models
{
    public class Reporte
    {
        [Key]
        public int Id { get; set; }
        public int IdEmpleado { get; set; }
        public string Empleado { get; set; }
        public string Rol { get; set; }
        public int HorasTrabajadas { get; set; }
        public int BonoEntrega { get; set; }
        public int BonoPorHora { get; set; }
        public float Isr { get; set; }
        public float IsrAdicional { get; set; }
        public float Vales { get; set; }

    }

   
}
