﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace RinkuBackend.Models
{
    [Table("CatBonosPorEntrega")]
    public partial class CatBonosPorEntrega
    {
        [Key]
        public int IdBonoEntrega { get; set; }
        public decimal Bono { get; set; }
        public DateTime FechaInicio { get; set; }
        public DateTime FechaFin { get; set; }
        public bool Activo { get; set; }
    }
}
