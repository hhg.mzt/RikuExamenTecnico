﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace RinkuBackend.Models
{
    [Table("CatIsrCalculos")]
    public partial class CatIsrCalculo
    {
        [Key]
        public int IdIsr { get; set; }
        public decimal FactorSimple { get; set; }
        public decimal FactorAdicional { get; set; }
        public short SueldoParaAplicarAdional { get; set; }
        public DateTime FechaInicio { get; set; }
        public DateTime FechaFin { get; set; }
    }
}
