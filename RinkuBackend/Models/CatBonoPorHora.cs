﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace RinkuBackend.Models
{
    [Table("CatBonoPorHora")]
    public partial class CatBonoPorHora
    {
        [Key]
        public int IdBonoHora { get; set; }
        public int IdRol { get; set; }
        public decimal Bono { get; set; }
        public DateTime FechaInicio { get; set; }
        public DateTime FechaFin { get; set; }
        public bool Activo { get; set; }
    }
}
