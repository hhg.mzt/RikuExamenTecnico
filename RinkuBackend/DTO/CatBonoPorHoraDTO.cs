﻿namespace RinkuBackend.DTO
{
    public class CatBonoPorHoraDTO
    {
        public int IdRol { get; set; }
        public double Bono { get; set; }
        public DateTime FechaInicio { get; set; }
        public DateTime FechaFin { get; set; }
        public bool Activo { get; set; }
    }

    public class CatBonoPorHoraReadDTO : CatBonoPorHoraDTO
    {
        public int IdBonoHora { get; set; }
    }

}
