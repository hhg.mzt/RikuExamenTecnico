﻿namespace RinkuBackend.DTO
{
    public class CatEmpleadoDTO
    {
        public int Numero { get; set; }
        public string NombreCompleto { get; set; }
        public int IdRol { get; set; }
    }
    public class CatEmpleadoReadDTO: CatEmpleadoDTO
    {
        public int IdEmpleado { get; set; }
    }
}
