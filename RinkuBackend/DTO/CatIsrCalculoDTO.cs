﻿namespace RinkuBackend.DTO
{
    public class CatIsrCalculoDTO
    {
        public decimal FactorSimple { get; set; }
        public decimal FactorAdicional { get; set; }
        public short SueldoParaAplicarAdional { get; set; }
        public DateTime FechaInicio { get; set; }
        public DateTime FechaFin { get; set; }
    }

    public class CatIsrCalculoReadDTO: CatIsrCalculoDTO
    {
        public int IdIsr { get; set; }
    }
}
