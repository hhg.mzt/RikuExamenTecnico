﻿namespace RinkuBackend.DTO
{
    public class PagoMensualDTO
    {
        public int IdMovimiento { get; set; }
        public decimal SueldoMensual { get; set; }
        public decimal TotalBonoEntrega { get; set; }
        public decimal TotalBonoHora { get; set; }
        public decimal TotalIsr { get; set; }
        public decimal TotalIsrAdicional { get; set; }
        public decimal? TotalVales { get; set; }
        public int HorasTrabajadas { get; set; }
    }

    public class PagoMensualReadDTO : PagoMensualDTO
    {
        public int IdPagoMensual { get; set; }
    }
}
