﻿namespace RinkuBackend.DTO
{
    public class MovimientoDTO
    {
        public int IdEmpleado { get; set; }
        public byte Mes { get; set; }
        public short Year { get; set; }
        public short CantidadEntregas { get; set; }
    }

    public class MovimientoReadDTO : MovimientoDTO
    {
        public int IdMovimiento { get; set; }
    }

}
