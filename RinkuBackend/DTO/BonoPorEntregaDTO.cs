﻿namespace RinkuBackend.DTO
{
    public class BonoPorEntregaDTO
    {
        public decimal Bono { get; set; }
        public DateTime FechaInicio { get; set; }
        public DateTime FechaFin { get; set; }
        public bool Activo { get; set; }
    }
    public class BonoPorEntegaReadDTO : BonoPorEntregaDTO
    {
        public int IdBonoEntrega { get; set; }
    }

}
