﻿namespace RinkuBackend.DTO
{
    public class CatRoleDTO
    {
        public string Descripcion { get; set; }
        public bool Activo { get; set; }
    }

    public class CatRoleReadDTO: CatRoleDTO
    {
        public int IdRol { get; set; }
    }
}
