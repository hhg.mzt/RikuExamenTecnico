﻿namespace RinkuBackend.DTO
{
    public class CatOtrasPrestacioneDTO
    {
        public string Descripcion { get; set; }
        public short Factor { get; set; }
        public bool SobreSueldo { get; set; }
        public bool AdiconalAsueldo { get; set; }
        public DateTime FechaInicio { get; set; }
        public DateTime FechaFin { get; set; }
    }

    public class CatOtrasPrestacioneReadDTO: CatOtrasPrestacioneDTO
    {
        public short IdPrestacion { get; set; }
    }

}
