# Bienvenidos este repositorio corresponde a la solución del ejercicio anexo en el Documento PDF.


### IDES y herramientas utilizadas
 • El backend fue construido con C# bajo el framework dotnet 6.
 • Base de datos MSSQL,
 • EL frontend fue cnstruido en TypeScript, HTML, CSS todo bajo el framework Angular 14
 • En el caso del backend el editor utilizado fue Visual Studio 2022, pero opcionalmene se puede usar visual studio auxiliandose de la linea de comandos de dotnet.
 • Por su parte para codificar el frontend el editor fue Visual Studio.
 ### Arquitectura.
 
• En el caso del backend se utilizado una arquitectura en capas en el caso de la codificacion y para el flujo la arquitectura fue Modelo-Controlador. Se uso el patron repositorio para las peticiones a la base de datos, el patron servicio para vincular los controladores con el repositorio. Tambien se utilizo el patron DTO para evitar interactuar directamenta con los modelos de la bd.
    Dentro de la aqrtutectura y patrones usados se uso la inyeccion de depencia y los patros repostiorios se manejaron como singleton.
• En el caso del frontend la arquitectura fue modularidad, creando los modulos que hacen de la vista. Estos modulos cuenta con su propio archivo de estilos y de lógica. Se usaron servicios para manejar la peticiones al backend. Estos servicios son singletos los cuales se inyecta en el constructor del componente. 

 ### Mejoras entre las mejoras implemantadas.
 • POsibilidad de eliminar empleados.
 • Manejar los valores de calculos como datos guardados en tablas especiales.
 
 
 ### Consideraciones
 Dado que el ejercion solo contempla los CRUD de Empleados y Movimientos. Es poblar la informaión de ciertas tablas como requisio previo al desarrollo e implementación.
 
 
 ##### Datos de la tabla roles.
    GO
    SET IDENTITY_INSERT [dbo].[CatRoles] ON 
    GO
    INSERT [dbo].[CatRoles] ([IdRol], [Descripcion], [Activo]) VALUES (1, N'CHOFER', 1)
    GO
    INSERT [dbo].[CatRoles] ([IdRol], [Descripcion], [Activo]) VALUES (2, N'CARGADOR', 1)
    GO
    INSERT [dbo].[CatRoles] ([IdRol], [Descripcion], [Activo]) VALUES (3, N'AUXILIAR', 1)
    GO
    SET IDENTITY_INSERT [dbo].[CatRoles] OFF
    GO

 ##### Datos de bonos.
     GO
    SET IDENTITY_INSERT [dbo].[CatBonoPorHora] ON 
    GO
    INSERT [dbo].[CatBonoPorHora] ([IdBonoHora], [IdRol], [Bono], [FechaInicio], [FechaFin], [Activo]) VALUES (1, 1, CAST(10 AS Decimal(3, 0)), CAST(N'2023-01-01' AS Date), CAST(N'9999-12-31' AS Date), 1)
    GO
    INSERT [dbo].[CatBonoPorHora] ([IdBonoHora], [IdRol], [Bono], [FechaInicio], [FechaFin], [Activo]) VALUES (3, 2, CAST(5 AS Decimal(3, 0)), CAST(N'2023-01-01' AS Date), CAST(N'9999-12-31' AS Date), 1)
    GO
    INSERT [dbo].[CatBonoPorHora] ([IdBonoHora], [IdRol], [Bono], [FechaInicio], [FechaFin], [Activo]) VALUES (4, 3, CAST(0 AS Decimal(3, 0)), CAST(N'2023-01-01' AS Date), CAST(N'9999-12-31' AS Date), 1)
    GO
    SET IDENTITY_INSERT [dbo].[CatBonoPorHora] OFF
    GO
    SET IDENTITY_INSERT [dbo].[CatBonosPorEntrega] ON 
    GO
    INSERT [dbo].[CatBonosPorEntrega] ([IdBonoEntrega], [Bono], [FechaInicio], [FechaFin], [Activo]) VALUES (1, CAST(5 AS Decimal(3, 0)), CAST(N'2023-01-01' AS Date), CAST(N'9999-12-31' AS Date), 1)
    GO
    SET IDENTITY_INSERT [dbo].[CatBonosPorEntrega] OFF
    GO
 
  ##### Datos de tabla calculo de isr.
    SET IDENTITY_INSERT [dbo].[CatIsrCalculos] ON 
    GO
    INSERT [dbo].[CatIsrCalculos] ([IdIsr], [FactorSimple], [FactorAdicional], [SueldoParaAplicarAdional], [FechaInicio], [FechaFin]) VALUES (1, CAST(9 AS Numeric(3, 0)), CAST(3 AS Numeric(2, 0)), 10000, CAST(N'2023-01-01' AS Date), CAST(N'9999-12-31' AS Date))
    GO
    SET IDENTITY_INSERT [dbo].[CatIsrCalculos] OFF
    GO

  ##### Datos de tabla otras prestaciones (VALES DE DESPENSA).
  SET IDENTITY_INSERT [dbo].[CatOtrasPrestaciones] ON 
    GO
    INSERT [dbo].[CatOtrasPrestaciones] ([IdPrestacion], [Descripcion], [Factor], [SobreSueldo], [AdiconalASueldo], [FechaInicio], [FechaFin]) VALUES (1, N'VALES', 4, 0, 1, CAST(N'2023-01-01' AS Date), CAST(N'9999-12-31' AS Date))
    GO
    SET IDENTITY_INSERT [dbo].[CatOtrasPrestaciones] OFF
    GO
    SET IDENTITY_INSERT [dbo].[CatRoles] ON 
    GO

El esquema de las tablas a si como los procedimientos almacenados estan disponible dentro de la carpeta DATA del proyecto de backend.
